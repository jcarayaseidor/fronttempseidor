import { Component, OnInit } from '@angular/core';
import {SocketService} from '../services/socket.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
   cityArray;
  constructor(private socket: SocketService) { }

  ngOnInit() {
    setInterval(()=>{
      this.socket.sendMesg();
    },10000);
    this.socket.getCastUpdate().subscribe((data)=>{
      console.log(data);
          this.cityArray = data;
    });
  }

}
