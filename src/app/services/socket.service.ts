import { Injectable } from '@angular/core';
import * as socket from 'socket.io-client';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private url = 'http://localhost:3000';
  private socket;

  constructor() {
    this.socket = socket(this.url);
  }

  sendMesg() {
    this.socket.emit('update', 'Actualizar cast.....');
  }

  getCastUpdate(){
     return new Observable((observer)=>{
       this.socket.on('cityData',(data)=>{
         observer.next(data);
       });
     })

  }
}
